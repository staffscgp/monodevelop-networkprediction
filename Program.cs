﻿#region File Description
//-----------------------------------------------------------------------------
// PeerToPeerGame.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;

#if MONOMAC
using MonoMac.Foundation;
using MonoMac.AppKit;
using MonoMac.ObjCRuntime;
#elif IPHONE
using MonoTouch.Foundation;
using MonoTouch.UIKit;
#endif

#endregion


namespace NetworkPrediction
{
	
	#region Entry Point

	class Program
	{
		public static void Main(string[] args)
		{
			using (NetworkPrediction.NetworkPredictionGame game = new NetworkPrediction.NetworkPredictionGame())
			{
				game.Run();
			}
		}
	}
	
	#endregion
}
